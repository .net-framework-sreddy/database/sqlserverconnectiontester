﻿/*
 * Title:   SQLServer Database Connection Test Client
 * Project: SQLServerConnectionTester
 * File:    Program.cs
 * Author:  Sheldon Reddy
 * Date :   20190605
 * Desc:    
 * Usage:   Edit the connection string in the AppConfig File.
 *          Add the System.Configuration Reference to the project to import AppConfig key-value pairs
 *          
 * Status:  Complete
 * Errors:  None
 */ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// For Database Access
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Security.Cryptography.X509Certificates;
using System.Security;
using System.Data;
// For AppConfig
using System.Configuration;

namespace SQLServerConnectionTester
{
    class Program
    {
        #region Globals
        private static Dictionary<string, string> AppConfig;

        #endregion


        static void Main(string[] args)
        {

            #region ImportAppConfig

            #region Application Keys
            string[] AppConfigKeys = { "ConnStr" };
            #endregion

            #region Key-Value Assignment
            AppConfig = new Dictionary<string, string>();
            foreach (string key in AppConfigKeys)
            {
                try
                {
                    AppConfig.Add(key, ConfigurationManager.AppSettings[key]);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }

            #endregion

            #endregion

            #region Database Connection
            DBConnOps();
            #endregion

            Console.ReadLine();
        }

        #region DB Connection Operations
        public static void DBConnOps()
        {
            SqlConnection connection = null;
            try
            {
                connection = new SqlConnection(AppConfig["ConnStr"]);

                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    Console.WriteLine("Connection Success!");
                    connection.Close();
                    Console.WriteLine("Connection Closed");
                }
                else
                {
                    Console.WriteLine("DB Connection Failed!");
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                // TODO. Implement Server Reconnection Attempt here.
            }
        }
        #endregion

    }
}
